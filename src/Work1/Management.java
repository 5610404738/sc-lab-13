package Work1;

import java.util.LinkedList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Management {
	
	private Lock manageLock;
	private Condition manageCon;
	private LinkedList<Integer> link;
	private int nElement;
	
	public Management() {
		manageLock = new ReentrantLock();
		manageCon = manageLock.newCondition();
		link = new LinkedList<Integer>();
	}
	
	public void add() {
		manageLock.lock();
		try {
			link.add(1);
			System.out.println("Element add in LinkedList");
			manageCon.signalAll();
		}
		finally {
			manageLock.unlock();
		}
	}
	
	public void Del() throws InterruptedException {
		manageLock.lock();
		try {
			while(link.size()<1){manageCon.await();
			}
			link.removeLast();
			System.out.println("Element remove in LinkedList");
				
		}
		finally {
			manageLock.unlock();
		}
	}

	
}
