package Work2;

public class AddRunnable implements Runnable{
	private Contact c;
	private int count;
	private String n;
	private static int DELAY = 1;
	public AddRunnable(Contact c,int count,String n) {
		this.c = c;
		this.count = count;
		this.n = n;
		
	}
	
	@Override
	public void run() {
		try{
			for(int i=1;i<=count;i++){
				c.add(n);
				Thread.sleep(DELAY);
			}
		}catch(InterruptedException e){
			System.err.println("Error");
		}
	}

}
