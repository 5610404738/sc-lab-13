package Work2;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Contact {
	private Lock BalLock;
	private Condition enoughCon;
	private Queue q;

	public Contact() {
		BalLock = new ReentrantLock();
		enoughCon = BalLock.newCondition();
		q = new Queue(10);
	}

	public void add(String a) throws InterruptedException {
		try {
			BalLock.lock();

			while (q.getSize() > 10) {
				enoughCon.await();
			}
			q.add(a);

			System.out.print("Add " + a+"\n");
			System.out.println(q.toString());
			enoughCon.signalAll();
		} 
		finally {
			BalLock.unlock();
		}
	}

	public void remove(String a) throws InterruptedException {
		try {
			BalLock.lock();
			while (q.getSize() <= 0) {
				enoughCon.await();
			}
			q.remove(0);
			System.out.print("remove " + a+"\n");
			System.out.println(q.toString());
			enoughCon.signalAll();
		} 
		finally {
			BalLock.unlock();
		}
	}
}
