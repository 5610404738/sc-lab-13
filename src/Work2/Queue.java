package Work2;

import java.util.ArrayList;

public class Queue {
	private ArrayList<String> list;
	private int size;
	
	public Queue(int s){
		list = new ArrayList<String>();		
		this.size = s;
	}
	public int getSize(){
		return list.size();
	}	
	public void add(String n){
		list.add(n);
	}	
	public void remove(int n){
		list.remove(n);		
	}		
	public String toString(){
		return list.toString();
	}
}
