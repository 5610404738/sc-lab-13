package Work2;

public class RemoveRunnable implements Runnable{
	private Contact c;
	private int count;
	private String n;
	private static int DELAY = 1;
	
	public RemoveRunnable(Contact c,int count,String n){
		this.c = c;
		this.count = count;
		this.n = n;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		try{
			for(int i=1;i<=count;i++){c.remove(n);
				Thread.sleep(DELAY);
			}
		}catch(InterruptedException e){
			System.err.println("Error");
		}
	}
}
